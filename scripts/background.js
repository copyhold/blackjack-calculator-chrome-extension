'use strict';
var opened = false;
chrome.browserAction.onClicked.addListener(function(tab) {
  toggle_bjcalc();
});
chrome.runtime.onInstalled.addListener(function (details) {
  console.log('previousVersion', details.previousVersion);
});
chrome.extension.onMessage.addListener(function(msg){
  if (msg=='close bjcalc') close_bjcalc();    
});

function toggle_bjcalc() {
  if (opened) {
    close_bjcalc();
  } else {
    open_bjcalc();
  }
}
function close_bjcalc() {
    chrome.tabs.executeScript({'code': 'document.body.classList.remove("bjcalc")'});
    chrome.browserAction.setIcon({path: "images/icon48.png"});
    opened = false;
}
function open_bjcalc() {
    chrome.tabs.executeScript({'code': 'document.body.classList.add("bjcalc")'});
    chrome.browserAction.setIcon({path: "images/icon48-invert.png"});
    opened = true;
}
