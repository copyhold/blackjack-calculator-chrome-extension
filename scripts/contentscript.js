'use strict';
var e = document.createElement('iframe');
e.id = 'bjcalc';
e.setAttribute('src', chrome.extension.getURL('calculator.html'));
e.setAttribute('scrolling','no');
document.querySelector('body').appendChild(e);
window.addEventListener('message', function(m) {
  chrome.extension.sendMessage(m.data);
});
